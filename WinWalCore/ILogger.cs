﻿namespace WinWal
{
    public interface ILogger
    {
        void Log(string message);
    }
}
