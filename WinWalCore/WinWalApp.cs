﻿using System;
using System.Collections.Generic;
using System.IO;
using static WinWal.WinWalCore;

namespace WinWal
{
    public class WinWalApp<T> where T : ILogger, new()
    {
        public static readonly string APPNAME = Path.GetFileNameWithoutExtension(Environment.GetCommandLineArgs()[0]);

        private static readonly ILogger logger = new T();

        public static int Main(string[] args)
        {
            try
            {
                var app = ProcessArgs(args);
                return app.Run();
            }
            catch (WinWalAppArgumentException e)
            {
                if (!string.IsNullOrEmpty(e.Message))
                {
                    logger.Log(e.Message);
                }

                logger.Log("Exitting...");
                return e.ErrorCode;
            }
        }

        private static WinWalCore ProcessArgs(string[] args)
        {
            var sources = new List<DirectoryInfo>();
            var style = WallpaperStyle.NotSet;
            var clearAppData = false;

            if (args.Length < 1)
            {
                throw new WinWalAppArgumentException("No arguments given", 1);
            }

            int index = 0;
            string arg;
            string value;

            while (index < args.Length)
            {
                arg = args[index];

                switch (arg)
                {
                    case "-s":
                    case "--style":
                        try
                        {
                            value = args[++index];
                        }
                        catch (IndexOutOfRangeException)
                        {
                            throw new WinWalAppArgumentException($"'style' parameter requires a value", 2);
                        }

                        try
                        {
                            style = Enum.Parse<WallpaperStyle>(value);
                        }
                        catch (ArgumentException)
                        {
                            throw new WinWalAppArgumentException($"'{value}' is not a valid style", 2);
                        }

                        break;

                    case "-d":
                    case "--directory":
                        try
                        {
                            value = args[++index];
                        }
                        catch (IndexOutOfRangeException)
                        {
                            throw new WinWalAppArgumentException($"'directory' parameter requires a value", 3);
                        }

                        try
                        {
                            var info = new DirectoryInfo(value);

                            if (!info.Exists)
                            {
                                throw new WinWalAppArgumentException($"Error reading directory '{value}': Directory does not exist", 3);
                            }

                            sources.Add(new DirectoryInfo(value));
                        }
                        catch (Exception e)
                        {
                            throw new WinWalAppArgumentException($"Error reading directory '{value}': {e.Message}", e, 3);
                        }

                        break;

                    case "-c":
                    case "--clear-appdata":
                        clearAppData = true;
                        break;

                    case "-h":
                    case "--help":
                        Console.WriteLine(string.Join(Environment.NewLine, new string[]
                        {
                            "",
                            $"{APPNAME} - Recursively finds images and randomly set as wallpaper",
                            "",
                            $"Usage: {APPNAME} [options] [directories]",
                            "",
                            "Options:",
                            "    -s, --style [VALUE]     The style for the wallpaper to apply",
                            $"                            Possible values: {string.Join(", ", Enum.GetNames(typeof(WallpaperStyle)))}",
                            "    -d, --directory [VALUE] The directories to search through",
                            "    -c, --clear-appdata     Delete all files in the AppData folder",
                            "",
                        }));

                        throw new WinWalAppArgumentException("", 0);

                    default:
                        if (!arg.StartsWith("-"))
                        {
                            value = args[index];

                            try
                            {
                                var info = new DirectoryInfo(value);

                                if (!info.Exists)
                                {
                                    throw new WinWalAppArgumentException($"Error reading directory '{value}': Directory does not exist", 3);
                                }

                                sources.Add(new DirectoryInfo(value));
                            }
                            catch (Exception e)
                            {
                                throw new WinWalAppArgumentException($"Error reading directory '{value}': {e.Message}", e, 3);
                            }
                        }
                        else
                        {
                            throw new WinWalAppArgumentException($"Unknown Argument: {arg}", 1);
                        }

                        break;
                }

                index++;
            }

            return new WinWalCore(wallpaperSource: sources.ToArray(), style: style, clearAppData: clearAppData)
            {
                Logger = logger,
            };
        }
    }

    public class WinWalAppArgumentException : ArgumentException
    {
        public int ErrorCode { get; set; }

        public WinWalAppArgumentException(int errorCode = 0) : base()
        {
            ErrorCode = errorCode;
        }

        public WinWalAppArgumentException(string message, int errorCode = 0) : base(message)
        {
            ErrorCode = errorCode;
        }

        public WinWalAppArgumentException(string message, Exception inner, int errorCode = 0) : base(message, inner)
        {
            ErrorCode = errorCode;
        }
    }
}
