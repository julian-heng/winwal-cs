﻿using System;
using System.Runtime.InteropServices;

namespace WinWal
{
    public static class WinAPI
    {
        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern int SendMessageTimeout(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam, uint fuFlags, uint uTimeout, out IntPtr result);

        [Flags]
        public enum AD_Apply : int
        {
            SAVE = 0x00000001,
            HTMLGEN = 0x00000002,
            REFRESH = 0x00000004,
            ALL = SAVE | HTMLGEN | REFRESH,
            FORCE = 0x00000008,
            BUFFERED_REFRESH = 0x00000010,
        }

        [ComImport]
        [Guid("F490EB00-1240-11D1-9888-006097DEACF9")]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        public interface IActiveDesktop
        {
            [PreserveSig]
            int ApplyChanges(AD_Apply dwFlags);
            [PreserveSig]
            int GetWallpaper([MarshalAs(UnmanagedType.LPWStr)] System.Text.StringBuilder pwszWallpaper,
                             int cchWallpaper,
                             int dwReserved);
            [PreserveSig]
            int SetWallpaper([MarshalAs(UnmanagedType.LPWStr)] string pwszWallpaper, int dwReserved);
        }

        public class ActiveDesktop
        {
            private static readonly Guid CLSID_ActiveDesktop = new Guid("{75048700-EF1F-11D0-9888-006097DEACF9}");

            private static IActiveDesktop GetActiveDesktop()
            {
                Type typeActiveDesktop = Type.GetTypeFromCLSID(CLSID_ActiveDesktop);
                return (IActiveDesktop)Activator.CreateInstance(typeActiveDesktop);
            }

            private static void Enable()
            {
                IntPtr result = IntPtr.Zero;
                SendMessageTimeout(FindWindow("Progman", null), 0x52c, IntPtr.Zero, IntPtr.Zero, 0, 500, out result);
            }

            public static void SetWallpaper(string path)
            {
                Enable();
                IActiveDesktop iad = GetActiveDesktop();
                iad.SetWallpaper(path, 0);
                iad.ApplyChanges(AD_Apply.ALL | AD_Apply.FORCE | AD_Apply.BUFFERED_REFRESH);
                Marshal.ReleaseComObject(iad);
            }
        }
    }
}
