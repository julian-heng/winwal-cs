﻿using System;
using WinWal;

namespace WinWalGUI
{
    public class Program
    {
        [STAThread]
        public static int Main(string[] args)
        {
            return WinWalApp<Logger>.Main(args);
        }
    }

    public class Logger : ILogger
    {
        public void Log(string message)
        {
        }
    }
}
