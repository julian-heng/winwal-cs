﻿using Microsoft.Win32;
using StbImageSharp;
using StbImageWriteSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace WinWal
{
    public class WinWalCore
    {
        private static readonly HashSet<string> EXTENSIONS = new HashSet<string>
        {
            ".jpg",
            ".jpeg",
            ".png",
            ".bmp",
        };

        private static readonly HashSet<string> NEED_CONVERT = new HashSet<string>
        {
            ".png",
        };

        public enum WallpaperStyle
        {
            Tile,
            Center,
            Stretch,
            Fit,
            Fill,
            NotSet,
        }

        public static readonly Dictionary<WallpaperStyle, Tuple<string, string>> REGISTRY_MAP = new Dictionary<WallpaperStyle, Tuple<string, string>>()
        {
            {WallpaperStyle.Tile, new Tuple<string, string>("0", "1")},
            {WallpaperStyle.Center, new Tuple<string, string>("0", "0")},
            {WallpaperStyle.Stretch, new Tuple<string, string>("2", "0")},
            {WallpaperStyle.Fit, new Tuple<string, string>("6", "0")},
            {WallpaperStyle.Fill, new Tuple<string, string>("10", "0")},
        };

        public WallpaperStyle Style { get; set; }
        public DirectoryInfo[] WallpaperSource { get; set; }
        public string[] Existing { get; private set; } = Array.Empty<string>();
        public bool ClearAppData { get; set; }
        public ILogger Logger { get; set; } = null;

        public static string WinWalAppData => Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), @"WinWal");

        public WinWalCore(DirectoryInfo[] wallpaperSource = null, WallpaperStyle style = WallpaperStyle.NotSet, bool clearAppData = false)
        {
            WallpaperSource = wallpaperSource is null ? Array.Empty<DirectoryInfo>() : wallpaperSource;
            Style = style;
            ClearAppData = clearAppData;

            if (!Directory.Exists(WinWalAppData))
            {
                Directory.CreateDirectory(WinWalAppData);
            }

            Existing = Directory.EnumerateFiles(WinWalAppData, "*").Where(ValidateFile).ToArray();
        }

        public int Run()
        {
            var watch = Stopwatch.StartNew();
            if (ClearAppData)
            {
                Logger?.Log($"Clearing AppData folder ({WinWalAppData})");

                foreach (var file in Existing)
                {
                    Logger?.Log($"Deleting '{file}'");
                    File.Delete(file);
                }
            }
            else
            {
                var files = FindFiles();

                if (files.Length == 0)
                {
                    Logger?.Log("Can't find any images");
                    Logger?.Log("Exitting...");
                    return 2;
                }

                var file = files[new Random().Next(0, files.Length)];
                Logger?.Log($"Selecting '{file}'");

                var dest = PrepareWallpaper(file);
                Logger?.Log($"Destination is '{dest}'");

                ApplyWallpaperStyle();
                WinAPI.ActiveDesktop.SetWallpaper(dest);
            }
            watch.Stop();

            Logger?.Log($"Done! ({watch.ElapsedMilliseconds}ms)");

            return 0;
        }

        private string[] FindFiles()
        {
            string[] result = Array.Empty<string>();

            var opt = new EnumerationOptions
            {
                RecurseSubdirectories = true,
                IgnoreInaccessible = true,
            };

            foreach (var path in WallpaperSource)
            {
                var files = Directory.EnumerateFiles(path.FullName, "*", opt).Where(ValidateFile).ToArray();
                result = result.Concat(files).ToArray();
            }

            return result;
        }

        private string PrepareWallpaper(string path)
        {
            var filename = Path.GetFileNameWithoutExtension(path);
            var dest = Path.Combine(WinWalAppData, $@"{filename}.jpg");

            if (!Existing.Contains(dest))
            {
                Logger?.Log($"File '{dest}' does not exist.");

                if (!NEED_CONVERT.Contains(Path.GetExtension(path).ToLower()))
                {
                    Logger?.Log($"Copying '{path}' to '{dest}'");
                    File.Copy(path, dest);
                }
                else
                {
                    Logger?.Log($"Converting '{path}' to '{dest}'");
                    ConvertToJpg(path, dest);
                }
            }
            else
            {
                Logger?.Log($"File '{dest}' already exists.");
            }

            return dest;
        }

        private void ApplyWallpaperStyle()
        {
            if (Style == WallpaperStyle.NotSet)
            {
                Logger?.Log("Style is not set, leaving it be");
                return;
            }

            using var key = Registry.CurrentUser.OpenSubKey(@"Control Panel\Desktop", true);

            var currStyle = key.GetValue(@"WallpaperStyle") as string;
            var currTile = key.GetValue(@"TileWallpaper") as string;

            (var newStyle, var newTile) = REGISTRY_MAP[Style];

            if (currStyle.Equals(newStyle) && currTile.Equals(newTile))
            {
                Logger?.Log($"Style is already set to {Style}");
            }
            else
            {
                Logger?.Log($"Setting style to {Style} ({newStyle}, {newTile})");
                key.SetValue(@"WallpaperStyle", newStyle);
                key.SetValue(@"TileWallpaper", newTile);
            }
        }

        private void ConvertToJpg(string path, string dest)
        {
            using var inStream = File.OpenRead(path);
            using var outStream = File.OpenWrite(dest);
            var img = ImageResult.FromStream(inStream);

            ImageWriter imgWriter = new ImageWriter();
            imgWriter.WriteJpg(img.Data, img.Width, img.Height, (StbImageWriteSharp.ColorComponents)img.Comp, outStream, 95);
        }

        private bool ValidateFile(string filePath)
        {
            return EXTENSIONS.Contains(Path.GetExtension(filePath).ToLower());
        }
    }
}
