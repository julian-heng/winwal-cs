﻿using System;

namespace WinWal
{
    public class Program
    {
        [STAThread]
        public static int Main(string[] args)
        {
            return WinWalApp<Logger>.Main(args);
        }
    }

    public class Logger : ILogger
    {
        public void Log(string message)
        {
            Console.Error.WriteLine($"{WinWalApp<Logger>.APPNAME}: {message}");
        }
    }
}
